(defproject flocktory-challenge "0.1.0-SNAPSHOT"
  :description "Flocktory challenge"
  :url "https://bitbucket.org/iamakingbee/flocktory-challenge"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.3.442"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/tools.logging "0.3.1"]

                 [com.stuartsierra/component "0.3.2"]

                 ;; http api
                 [ring/ring-core "1.6.0-RC1"]
                 [ring/ring-json "0.5.0-beta1"]
                 [info.sunng/ring-jetty9-adapter "0.9.5"]

                 [clj-http "2.3.0"]

                 [com.google.guava/guava "21.0"]]

  :main ^:skip-aot flocktory-challenge.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
