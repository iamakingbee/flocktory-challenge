# flocktory-challenge

Описание задачи не прикладываю, так как репозиторий в паблике.

Меня просили оценку времени перед началом работы: она была час-полтора. Реально на решение задачи потратил около 2.5-3 часа (больше времени на бутстрап приложения и больше времени на воркер) + полчаса на последний коммит с причесыванием.

## Запуск

```bash
# В системе должен быть lein. Если его нет, ставить отсюда: https://leiningen.org/#install
# Поставить зависимости.
lein deps

# Запустить сервер на 8080 порту.
lein run

# Запустить с кастомным ограничением 
# количества одновременных соединений для клиента.
# По умолчанию стоит 4.
MAX_CONNECTIONS_PER_CLIENT=10 lein run
```

Чтобы получить pretty-printed ответ от API, нужно добавить в запрос query-параметр `pretty=1` или `pretty=true`.

## Как делал

- Сервис спроектирован на основе Component. Компоненты: сервер (Jetty), API-хендлер, коннекшн пул для http-клиента.
- API собрано на голом Ring. Pedestal или Compojure не брал, так как у нас один эндпойнт.
- Параллелизация сделана через core.async. Работает так:
  - Создаем воркер-пул размера `MAX_CONNECTIONS_PER_CLIENT`
  - Сливаем в канал все queries, полученные из api-хендлера
  - Воркеры эти queries фетчат и обрабатываеют, и кладут ответы в другой канал
  - Ответы из воркера собираются из канала в лупе и по ходу дела мерджатся между собой (`(merge-with + ...)`)
  - Когда работы больше нет, кладем результат в третий канал, откуда он отдается обратно в хендлер.
  - Если при обработке хотя бы одного запроса произойдет ошибка, мы вернем ее в хендлер, и API отдаст 500 с описанием этой ошибки.
- C xml работаем при помощи clojure.data.xml - он простой и lazy. Можно было бы использовать zippers, но задача достаточно проста, и я не стал их тащить.
- Общаемся с Yandex при помощи clj-http c persitent connection.

## Что хотел бы сделать / мог бы сделать лучше

- Написать тесты. В связи с нехваткой времени их писать я не стал.
- Вынести воркер в отдельный компонент, а на "клиенте" оставить только логику ограничения количества одновременных запросов.
- Сделать по-нормальному конфиг (компонент + протокол на каждом конфигурируемом компоненте)
- Лучше хендлить ошибки. Сейчас все ошибки из воркера просто сливаются обратно в API со статусом 500. По-хорошему, надо знать, какие ошибки мы там ловим, и, возможно, продумать разные сценарии для разных ошибок.
- Настроить логгинг
