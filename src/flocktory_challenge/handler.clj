(ns flocktory-challenge.handler
  (:require [flocktory-challenge
             [fetcher :as fetcher]
             [parallelizer :refer [parallelize]]]
            [com.stuartsierra.component :as component]
            [ring.util.response :refer [status response]]
            [ring.middleware
             [params :refer [wrap-params]]
             [keyword-params :refer [wrap-keyword-params]]
             [json :refer [json-response]]]))

;; a dirty hack. it's better to get a proper config library
;; like aviso.config for this kind of stuff
;; and move config to a separate component
(def max-connections
  (if-let [val (System/getenv "MAX_CONNECTIONS_PER_CLIENT")]
    (Integer. (re-find #"\d+" val))
    4))

(defn- get-counts-for-queries
  ([queries]
   (get-counts-for-queries queries {}))
  ([queries opts]
   (let [resp (if (= 1 (count queries))
                (fetcher/get-counts (first queries))
                (parallelize fetcher/get-counts
                             queries
                             (merge opts {:max-threads max-connections
                                          :merge-fn (partial merge-with +)})))]
     (if (instance? Throwable resp)
       (-> (response {:error (.getMessage resp)})
           (status 500))
       (-> (response resp)
           (status 200))))))

(defn handle-search
  [{{:keys [query] :as params} :params pool :pool :as req}]
  (if (empty? query)
    (-> (response {:error "query must not be empty"})
        (status 422))
    (let [queries (if (coll? query)
                    query
                    [query])]
      (-> (distinct queries)
          (get-counts-for-queries {:pool pool})))))

(defn handler
  [{:keys [uri] :as req}]
  (if (= "/search" uri)
    (handle-search req)
    (-> (response {:error "not found"})
        (status 404))))

(defn- wrap-json-response
  "Pretty prints JSON if request
   has ?pretty=1 query parameter"
  [handler]
  (fn [{{:keys [pretty] :as params} :params :as req}]
    (let [json-opts (if (some? (#{"1" "true"} pretty))
                      {:pretty true}
                      {})]
      (json-response (handler req) json-opts))))

(defn- wrap-component-deps
  "Inject component deps into request"
  [handler deps]
  (fn [req]
    (handler (merge req deps))))

(defn make-app [deps]
  (-> handler
      (wrap-component-deps deps)
      (wrap-json-response)
      (wrap-keyword-params)
      (wrap-params)))

(defrecord Handler [app pool]
  component/Lifecycle

  (start [this]
    (if app
      this
      (assoc this
             :app
             (make-app {:pool (:pool pool)}))))

  (stop [this]
    (if app
      (assoc this :app nil)
      this)))
