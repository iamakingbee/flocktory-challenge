(ns flocktory-challenge.connection-pool
  (:require [clj-http.conn-mgr :as pool]
            [com.stuartsierra.component :as component]))

(defrecord ConnectionPool [pool]
  component/Lifecycle

  (start [this]
    (if pool
      this
      (assoc this :pool (pool/make-reusable-conn-manager {:timeout 2
                                                          :threads 4}))))
  (stop [this]
    (if pool
      (pool/shutdown-manager pool)
      this)))
