(ns flocktory-challenge.parallelizer
  (:require [clojure.core.async :refer [<! go >! chan
                                        thread close!
                                        go-loop alts!!]]))

(def ^:const default-max-threads 4)

(defn parallelize
  "Calls an f against each element of coll
   in a limited worker pool, then merges results."
  ([f coll]
   (parallelize f coll {}))
  ([f coll {:keys [max-threads merge-fn]
            :or {max-threads default-max-threads
                 merge-fn merge}
            :as opts}]
   (let [queue-ch (chan) ;; we're okay with buffer-size==1 here
         resp-ch (chan)
         error-ch (chan)
         finish-ch (chan)]

     (go
       (loop [coll coll result {}]
         (if (seq coll)
           (let [resp (<! resp-ch)]
             (if (instance? Throwable resp)
               (do (close! queue-ch)
                   (>! error-ch resp))
               (recur (rest coll)
                      (merge-fn result resp))))
           (>! finish-ch result))))

     (go
       (doseq [x coll]
         (>! queue-ch x))
       (close! queue-ch))

     ;; worker pool, number of workers is limited by max-connections
     (dotimes [n (min (count coll) max-threads)]
       (go-loop []
           (when-let [x (<! queue-ch)]
             (>! resp-ch (<! (thread (f x opts))))
             (recur))))

     (let [[result err] (alts!! [finish-ch error-ch])]
       (close! resp-ch)
       (close! resp-ch)
       (or result err)))))
