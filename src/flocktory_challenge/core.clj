(ns flocktory-challenge.core
  (:require [flocktory-challenge
             [handler :refer [map->Handler]]
             [server :refer [map->Server]]
             [connection-pool :refer [map->ConnectionPool]]]
            [com.stuartsierra.component :as component])
  (:gen-class))

(defn- add-shutdown-hook!
  [f]
  (doto (Runtime/getRuntime)
    (.addShutdownHook (Thread. f))))

(def system
  (component/system-map
   :pool   (map->ConnectionPool {})
   :handler (component/using
             (map->Handler {})
             [:pool])
   :server (component/using
            (map->Server {})
            [:handler])))

(defn -main
  [& args]
  (let [system (component/start system)
        lock   (promise)]
    (add-shutdown-hook! #(component/stop system))
    (add-shutdown-hook! #(deliver lock :done))
    @lock
    (System/exit 0)))
