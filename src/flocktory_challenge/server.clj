(ns flocktory-challenge.server
  (:require [flocktory-challenge.handler :as handler]
            [ring.adapter.jetty9 :as jetty]
            [com.stuartsierra.component :as component]))

(defrecord Server [server handler]
  component/Lifecycle

  (start [this]
    (if server
      this
      (let [app (:app handler)
            server (jetty/run-jetty app {:port 8080
                                         :host "0.0.0.0"
                                         :join? false})]
        (assoc this :server server))))

  (stop [this]
    (if server
      (jetty/stop-server server)
      this)))
