(ns flocktory-challenge.fetcher
  (:require [clj-http.client :as client]
            [clojure.tools.logging :as log]
            [clojure.data.xml :as xml])
  (:import [java.net URLEncoder URL]
           [java.lang IllegalStateException]
           [com.google.common.net InternetDomainName]))

(def ^:const max-entries 10)

(defn debug [x]
  (log/debug x)
  x)

(defn- make-url
  "Makes request url with query string"
  [query]
  (let [query (URLEncoder/encode query)]
    (format "https://yandex.ru/blogs/rss/search?text=%s" query)))

(defn- fetch-feed
  [url {:keys [pool] :as opts}]
  (client/get url
              (cond-> {:follow-redirects true
                       :socket-timeout 1000
                       :conn-timeout 2000}
                pool (assoc :connection-manager pool))))

(defn- get-link
  "Gets a link from RSS-entry"
  [entry]
  (->> entry
       (:content)
       (filter #(= (:tag %) :link))
       (map :content)
       (ffirst)))

(defn- get-domain
  "Gets a domain name from link"
  [link]
  (let [host (.getHost (java.net.URL. link))
        domain (InternetDomainName/from host)]
    (str
     (try
       (if (.isTopPrivateDomain domain)
         domain
         (.topPrivateDomain domain))
       (catch IllegalStateException e
         domain)))))

(defn- parse-entries
  "Parses RSS returned from yandex"
  [resp]
  (->> resp
       (:body)
       (xml/parse-str)
       (:content)
       (first)
       (:content)
       (filter #(= (:tag %) :item))
       (take max-entries)))

(defn- calculate-counts
  "Calculates domain name frequencies"
  [resp]
  (let [xf (comp (map get-link)
                 (map get-domain))]
    (frequencies (->> resp
                      (parse-entries)
                      (transduce xf conj)))))

(defn get-counts
  "Gets and calculates domain name counts from a feed.
   Returns an empty map in case of API timeout."
  ([query]
   (get-counts query {}))
  ([query opts]
   (try
     (or (some-> query
                 (make-url)
                 (fetch-feed opts)
                 (calculate-counts))
         {})
     (catch Exception e
       (log/error (format "Caught exception while fetching query %s" query) e)
       e))))
